# Project 2 (Distributed Node-red Flows)

## Repository structure
The proposed solution is given as a Vagrantfile configurable via `config.rb`.

## Solution Architecture
The `Vagrantfile` will generate a configurable number of instances, which can be grouped in two types:
* `kafka` nodes, which will run a preconfigured kafka cluster (kafka is started via `systemd` with the `kafka.service`, zookeeper will automatically start kafka dependency as `zookeeper@kafka.service`)
* `node-red` nodes, which is an installation with a node-red instance exposed at port 1880.
  The service will run as a `vagrant` user systemd service (`node-red.service`) 

By default, the kafka cluster will enable automatic topic creation with as many partitions as nodes.
This is to simplify the POC, in a production environment it may be preferable to define a priori the topics and partitions.


### Vagrant configuration
We included a well commented configuration file `config.rb` that expose some parameters regarding the instances details, such as
* number of instances (both for kafka and node-red)
* CPU/Memory provisioning
* Networking

### Vagrant provisioning
The instances will be started according to `config.rb`

All instances use `archlinux/archlinux` as base image.

The provisioning will consist in several scripts (all defined in provision/scripts):
* at every startup, for all instances, vagrant will provision `sync_hosts`,
  which is a simple script that update `/etc/hosts` with the reference of all machines IPs
  (NB: there are some vagrant plugins that accomplish the same, but were mostly broken and outdated)
* for the first setup, all instances will run `setup_basic` provision, which update the system 
  and install some utility on the machine (git, vim, yay...)
* for **kafka** instances: 
  * execute `setup_kafka` provision, which will install kafka and setup the zookeper cluster
  * upload the source for the node-red healthcheck to the kafka nodes (`upload_kafka_healthcheck` provision)
  * execute `setup_kafka_healthcheck` which will install the healthcheck utility
* for **node-red** instances:
  * execute `setup_node_red` provision, which will install node-red, create a systemd user units for node-red startup, enable and run the unit.

### Node-red comunication
Communication between node-red instances is on top of the kafka cluster:
with kafka topics we can archive with no difficulty broadcast communication due the pub/sub pattern.

Setting up consumers with the same group id, we can archive an anycast communication.

Setting up ad-hoc topics for point-to-point communication (which in the case of the poc is extremely simple, since the topic is created if not present if a producer writes).


### Node-red liveness
The liveness is tested using a kafka topic, where the node-red instances have to actively report their liveness.

The healthcheck script will check the healthcheck topic and check the heartbeats of the last `-l <seconds>` (default 30) seconds. 

## Start the project

```sh
vagrant up
```

## Proof of concept

In order to run a demo, we prepared some already made flows:
* writer.json: will write on two topic (`timestamp` and `counter`)
  * the `counter` will be written in three partition with round robin logic.
* group-reader.json: will read from the counter topic with a specific group id (it is supposed to be deployed on different nodes)
* reader.json: will read from **both** topics, no groups (just basic debug)
