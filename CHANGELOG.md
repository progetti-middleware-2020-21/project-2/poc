# Changelog

## v0.x.y

### v0.0.1
Create "barebone" poc:
* the user need just to "vagrant up"
* some parameter are tunable via `./config.rb`
* `kafka` cluster works
* `node-red` has a basic node-red installation + nginx rev-proxy, in order to access the panel directly with the ip assigned by the host (default node-red is only accessible via localhost) 