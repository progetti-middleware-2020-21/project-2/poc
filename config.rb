################################
### Cluster configuration
################################

### Number of istances
$KAFKA_NODES = 3
$NODE_RED_NODES = 4

### CPU/Memory of each instance
# CPU
$KAFKA_CPU = 1
$NODE_RED_CPU = 1
# Memory
$KAFKA_MEMORY = 1024
$NODE_RED_MEMORY = 2048

### Networking
## To simplify the Vagrantfile and since during the poc
## there will be no reason to start more than a dozen instances,
## for networking the first three bytes of IPv4 must be specified as "fixed"
## and the other variables (they will start from $<VAR>_HOST, incrementing by instance)
##
##     ex:
##      $KAFKA_NODES = 3
##      $KAFKA_BASE_IP = "10.42.100."
##      $KAFKA_STARTING_HOST = 10
##
##      will generate
##      kafka1 10.42.100.10
##      kafka2 10.42.100.11
##      kafka3 10.42.100.12

# "fixed" first 3 bytes + the final point
#
#      ex:
#        $KAFKA_BASE_IP = "10.42.100."
#
$KAFKA_BASE_IP = "10.42.100."
$NODE_RED_BASE_IP = "10.42.101."


# starting host
$KAFKA_STARTING_HOST = 10
$NODE_STARTING_HOST = 10

# subnet mask of the network interface
$KAFKA_NETMASK = "255.255.0.0"
$NODE_RED_NETMASK = "255.255.0.0"

### kafka configuration
# automatic topic creation
$KAFKA_ENABLE_AUTO_TOPICS=true
# default number of partition for automatic topic creation
$KAFKA_DEFAULT_PARTITIONS=$KAFKA_NODES
