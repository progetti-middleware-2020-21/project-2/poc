require './config.rb'

$KAFKA_NODE_LIST = []
(1..$KAFKA_NODES).each do |kafka_id|
  $KAFKA_NODE_LIST.append("kafka" + "#{kafka_id}")
end

$NODE_RED_NODE_LIST = []
(1..$NODE_RED_NODES).each do |node_red_id|
  $NODE_RED_NODE_LIST.append("node-red" + "#{node_red_id}")
end


Vagrant.configure("2") do |config|
    
  config.vm.box = "archlinux/archlinux"
  config.vm.box_version = "20210215.15590"

  # kafka nodes config
  $KAFKA_NODE_LIST.each_with_index do |name, i|
    config.vm.define name do |kafka|
      
      kafka.vm.provider :libvirt do |libvirt|
          libvirt.cpus = $KAFKA_CPU
          libvirt.memory = $KAFKA_MEMORY
      end
      
      kafka.vm.hostname = name
      kafka.vm.network :private_network, ip: $KAFKA_BASE_IP + "#{i + $KAFKA_STARTING_HOST}", netmask: $KAFKA_NETMASK

      kafka.vm.provision "setup_kafka", type: "shell", privileged: false, path: 'provision/scripts/setup_kafka.sh',  :env => {
         "NODE_ID": "#{i + 1}",
         "N_NODES": "#{$KAFKA_NODES}",
         "KAFKA_ENABLE_AUTO_TOPICS": $KAFKA_ENABLE_AUTO_TOPICS,
         "KAFKA_DEFAULT_PARTITIONS": $KAFKA_DEFAULT_PARTITIONS
      }

      kafka.vm.provision "upload_kafka_healthcheck", type: "file", source: "healthcheck", destination: "/home/vagrant/healthcheck"
      kafka.vm.provision "setup_kafka_healthcheck", type: "shell", privileged: false, path: 'provision/scripts/setup_kafka_healthcheck.sh'
    end
  end
  
  # Node-red configs
  $NODE_RED_NODE_LIST.each_with_index do |name, i|
    config.vm.define name do |node_red|
      
      node_red.vm.provider :libvirt do |libvirt|
          libvirt.cpus = $NODE_RED_CPU
          libvirt.memory = $NODE_RED_MEMORY
      end
      
      node_red.vm.hostname = name
      node_red.vm.network :private_network, ip: $NODE_RED_BASE_IP + "#{i + $NODE_STARTING_HOST}", netmask: $NODE_RED_NETMASK

      # This folder should be (eventually) used to push to node-red instances some default config (for ex: the addresses of the kafka nodes)
      config.nfs.verify_installed = false ## in order to disable NFS and stop Vagrant NFS to be dumb (https://github.com/vagrant-libvirt/vagrant-libvirt/issues/900#issuecomment-411284849)
      config.vm.synced_folder './shared/node-red', '/vagrant', type: 'nfs', nfs_version: 4, disabled: true

      node_red.vm.provision "setup_node_red", type: "shell", privileged: false, path: 'provision/scripts/setup_node_red.sh'
    end
  end


  # /etc/hosts provision
  config.vm.provision "sync_hosts", type: "shell", :run => 'always', privileged: true, path: 'provision/scripts/sync_hosts.sh', :args => [
      $KAFKA_BASE_IP, $KAFKA_NODES, $KAFKA_STARTING_HOST,
      $NODE_RED_BASE_IP, $NODE_RED_NODES, $NODE_STARTING_HOST]

  # basic common tools install
  config.vm.provision "setup_basic", type: "shell", privileged: false, path: 'provision/scripts/setup.sh'

end