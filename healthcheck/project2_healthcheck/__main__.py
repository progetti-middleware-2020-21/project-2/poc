import sys
from datetime import datetime

from kafka import KafkaConsumer, TopicPartition
import time

import logging

from project2_healthcheck.cli import init_cli

parser = init_cli()
args = parser.parse_args()

if args.very_verbose:
    logging.basicConfig(level=logging.DEBUG)
elif args.verbose:
    logging.basicConfig(level=logging.INFO)

consumer = KafkaConsumer(bootstrap_servers=','.join(args.bootstrap_servers), value_deserializer=lambda x: x.decode('utf-8'), consumer_timeout_ms=1000)

# seeking to messages after the threshold
ts = int(time.time() * 1000)
ts_seek = ts - args.alive_threshold * 1000

logging.info(f'Current timestamp: {ts}')
logging.info(f'Searching in topic {args.topic} messages after timestamp: {ts_seek}')

print("Checking alive instances (last heartbeat before: {before}, topic: {topic})...".format(
    before=datetime.fromtimestamp(ts_seek/1000).strftime('%Y-%m-%d %H:%M:%S'),
    topic=args.topic
))

partitions = consumer.partitions_for_topic(args.topic)
if partitions is None:
    print(f'The healthcheck topic "{args.topic}" hasn\'t been found...')
    sys.exit(1)
offsets = consumer.offsets_for_times({TopicPartition(args.topic, k): ts_seek for k in partitions})

heartbeats = {}
for partition, offset in offsets.items():

    # for each partition, seek the correct offset for ts_seek
    # if None or <= 0, then the partition doesn't contain anything we care

    consumer.assign([partition])
    if offset is not None and offset[0] > 0:
        consumer.seek(partition, offset[0])
    else:
        continue

    # if seeked to the current offset, then proceed to process the partition until ts,
    # we have to stop to ts because if the topic and the partition are "flooded" by heartbeat,
    # the consumer_timeout_ms=1000 will not stop automatically the connection
    #
    # it StopIteration is thrown by consumer it means that there were no new heartbeat in the current
    # TopicPartition for consumer_timeout_ms and the last hearbeat.timestamp was < ts, which is actually not an error
    try:
        for heartbeat in consumer:
            if heartbeat.timestamp <= ts:
                heartbeats[heartbeat.value] = heartbeat.timestamp
            else:
                break
    except StopIteration:
        pass


# show the results

if len(heartbeats) == 0:
    print("No living instances found...")

for name, last_heartbeat in heartbeats.items():
    last_heartbeat = datetime.fromtimestamp(last_heartbeat/1000).strftime('%Y-%m-%d %H:%M:%S')
    print(f"{name}: {last_heartbeat}")