import argparse

DEFAULT_TOPIC = 'healthcheck'
DEFAULT_ALIVE_THRESHOLD = 30

def init_cli() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog='Project 2 - Healthcheck')

    parser.add_argument('-t', '--topic', type=str, default=DEFAULT_TOPIC, help=f'Topic where to check heart beats (default: {DEFAULT_TOPIC})')
    parser.add_argument('-l', '--alive-threshold', type=int, default=DEFAULT_ALIVE_THRESHOLD, help=f'Threshold after which the node is considered unhealty (default {DEFAULT_ALIVE_THRESHOLD}s)')
    parser.add_argument('-v', '--verbose', default=False, action='store_true', help='Verbose mode')
    parser.add_argument('-vv', '--very-verbose', default=False, action='store_true', help='Ultre verbose mode')

    parser.add_argument('-b', '--bootstrap-servers', nargs='+', metavar='kafka_broker', type=str, required=True, help='Kafka broker servers')

    return parser
