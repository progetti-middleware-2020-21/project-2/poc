#!/bin/bash

SYNC_HOST_MARKER="vagrant-sync_host-provisioned"
TARGET=/etc/hosts

PREFIX_KAFKA="$1"
N_KAFKA="$2"
STARTING_HOST_KAFKA="$3"

PREFIX_NODE_RED="$4"
N_NODE_RED="$5"
STARTING_HOST_NODE_RED="$6"

echo "Cleanup old generated /etc/hosts"
sed -i "/$SYNC_HOST_MARKER/d" "$TARGET"

echo "Provisioning /etc/hosts for \"$(cat /etc/hostname)\", inserting kafka{1..$N_KAFKA} and worker{1..$N_NODE_RED}"

function add_entry(){
    echo "Adding entry in /etc/hosts: $1 $2"
    echo "$1 $2 #$SYNC_HOST_MARKER" >> $TARGET
}

for kafka_id in $(seq 1 $N_KAFKA);do
    add_entry "$PREFIX_KAFKA$((STARTING_HOST_KAFKA-1+kafka_id))" kafka$kafka_id
done

for node_red_id in $(seq 1 $N_NODE_RED);do
    add_entry "$PREFIX_NODE_RED$((STARTING_HOST_NODE_RED-1+node_red_id))" node-red$node_red_id
done
