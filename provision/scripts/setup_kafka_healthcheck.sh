#!/bin/bash
cd ~/healthcheck
python -m venv venv
. venv/bin/activate

pip install -r requirements.txt
deactivate


cat <<EOF > ~/healthcheck/healthcheck.sh
cd ~/healthcheck
. venv/bin/activate
python -m project2_healthcheck "\$@"
EOF

chmod +x ~/healthcheck/healthcheck.sh

