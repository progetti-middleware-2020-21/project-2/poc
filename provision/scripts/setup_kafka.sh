#!/bin/bash

### installing kafka
yes | yay -S kafka python python-pip --noconfirm

sudo systemctl stop kafka
sudo systemctl stop zookeeper@kafka.service

### configuring zookeeper

# cleanup old logs
echo -n "" | sudo tee /var/log/zookeeper/zookeeper-kafka/zookeeper.log

echo "$NODE_ID" | sudo tee /var/lib/zookeeper/zookeeper-kafka/myid

echo "# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# the directory where the snapshot is stored.
dataDir=/var/lib/zookeeper/zookeeper-kafka
# the port at which the clients will connect
clientPort=2181
# disable the per-ip limit on the number of connections since this is a non-production config
maxClientCnxns=0
# interval between heartbeat
tickTime=1000

initLimit=10
syncLimit=4

# Disable the adminserver by default to avoid port conflicts.
# Set the port to something non-conflicting if choosing to enable this
admin.enableServer=true
admin.serverPort=8081" | sudo tee /etc/zookeeper/zookeeper-kafka/zoo.cfg

for NODE in $(seq 1 "$N_NODES"); do
    if [[ "$NODE" -eq "$NODE_ID" ]]; then
        echo "server.$NODE=0.0.0.0:2888:3888" | sudo tee -a /etc/zookeeper/zookeeper-kafka/zoo.cfg
    else
        echo "server.$NODE=kafka$NODE:2888:3888" | sudo tee -a /etc/zookeeper/zookeeper-kafka/zoo.cfg
    fi
done

### configuring kafka
# override of the systemd unit
sudo cp /usr/lib/systemd/system/kafka.service /etc/systemd/system/kafka.service
# for some reason, kafka crashed waiting the zookeper cluster, so we add a restart on failure
sudo sed -i "s/\[Service\]/[Service]\nRestart=on-failure\nRestartSec=15s/" \
    /etc/systemd/system/kafka.service
sudo sed -i "s/\[Unit\]/[Unit]\nStartLimitIntervalSec=300\nStartLimitBurst=20/" \
    /etc/systemd/system/kafka.service
sudo systemctl daemon-reload

sudo sed -i "s/broker\.id=[0-9]*/broker.id=$NODE_ID/" /etc/kafka/server.properties

# enabling/disablig automatic topic creation
sed '/^auto\.create\.topics\.enable=/d' /etc/kafka/server.properties ## cleanup
echo "auto.create.topics.enable=$KAFKA_ENABLE_AUTO_TOPICS" | sudo tee -a /etc/kafka/server.properties

# setup default number of partitions for automatic topic
sed '/^num\.partitions=/d' /etc/kafka/server.properties ## cleanup
echo "num.partitions=$KAFKA_DEFAULT_PARTITIONS" | sudo tee -a /etc/kafka/server.properties

# cleanup old kafka data/logs to avoid conflicts
sudo rm -rf /var/lib/kafka/*

### start and enable kafka (zookeeper will start/enable automatically as zookeeper@kafka.service)
sudo systemctl enable kafka
sudo systemctl start kafka