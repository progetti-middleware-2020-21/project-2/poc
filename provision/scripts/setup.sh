#!/bin/sh -x

# installing yay
sudo pacman -Sy --noconfirm
sudo pacman -S base-devel git --noconfirm 
cd /tmp
rm -rf yay
git clone https://aur.archlinux.org/yay.git
cd yay
yes | makepkg -si
cd
rm -rf /tmp/yay

# install other utilities
sudo pacman -S bash curl neovim man-db tmux dnsutils gnu-netcat --noconfirm

# just to have vim and vi to point to nvim
yay -S neovim-symlinks --noconfirm