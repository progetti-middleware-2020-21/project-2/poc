#!/bin/bash

cd $HOME

### install nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash

### manually load nvm
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

### install nodejs 15
nvm install 15

### install node-red and plugins
npm i -g node-red
mkdir -p $HOME/.node-red
cd $HOME/.node-red

npm i node-red-contrib-kafka-manager node-red-contrib-counter node-red-contrib-msg-router

### create a start-up script for node-red
mkdir -p ~/.bin
cat <<EOF > ~/.bin/node-red.sh
#!/bin/bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

node-red
EOF
chmod +x ~/.bin/node-red.sh

### creating custom systemd unit for node-red
mkdir -p ~/.config/systemd/user/

cat <<EOF > ~/.config/systemd/user/node-red.service
[Unit]
Description=Node-red systemd user unit

[Service]
Type=simple
StandardOutput=journal
ExecStart=/bin/bash -l /home/vagrant/.bin/node-red.sh
Environment="HOME=$HOME"

[Install]
WantedBy=default.target
EOF

### spawn a systemd for vagrant user
sudo loginctl enable-linger vagrant

### start/enable systemd unit
systemctl --user enable node-red
systemctl --user start node-red



